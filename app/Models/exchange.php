<?php

namespace App\Models;

use Cassandra\Bigint;
use Illuminate\Database\Eloquent\Model;


/**
 * @property Bigint id
 * @property int ramzinex_currency_id
 * @property string exchange_currency_symbol

 */
class exchange extends Model
{
    protected $table = 'Exchange';
    protected $fillable = [
        'id',
        'exchange_id',
        'name',
        'created_at',
        'updated_at',
    ];
}
