<?php

namespace App\Models;

use Cassandra\Bigint;
use Illuminate\Database\Eloquent\Model;


/**
 * @property Bigint id
 * @property int ramzinex_currency_id
 * @property string exchange_currency_symbol
 * @property Bigint ramzinex_balance
 * @property Bigint exchange_balance
 * @property Bigint exchange_balance_2
 * @property int ramzinex_volume_precision
 * @property int exchange_volume_precision
 * @property Bigint LFAT
 */
class Pair extends Model
{
    protected $table = 'Pair';
    protected $fillable =
        [
            'id',
            'pair_id', // it's always in ramzinex !!!
            'base_ENname',
            'base_FAname',
            'quote_ENname',
            'quote_FAname',
            'base_currency_id',
            'quote_currency_id', // always is rial or tether !!
            'base_precision',
            'quote_precision',
            'symbol',
            'commission_maker',
            'commission_taker',
            'exchange_id',
            'created_at',
            'updated_at',

        ];
}
