<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int currency_id
 * @property float amount
 * @property int time
 * @property string account_id
 */
class WithdrawDepositsCheckpoints extends Model
{
    /**
     * @var string
     */
    protected $table = 'withdraw_deposits_checkpoints';

    /**
     * @var string[]
     */
    protected $fillable = ['id', 'currency_id', 'amount', 'time','account_id'];

    /**
     * @var bool
     */
    public $timestamps = false;


}