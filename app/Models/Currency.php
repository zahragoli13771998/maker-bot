<?php

namespace App\Models;

use Cassandra\Bigint;
use Illuminate\Database\Eloquent\Model;


/**
 * @property Bigint id
 * @property int ramzinex_currency_id
 * @property string currency_symbol
 * @property Bigint balance
 * @property int ramzinex_volume_precision
 * @property Bigint LFAT
 */
class Currency extends Model
{
    protected $table = 'currencies';
    protected $fillable = [
        'id',
        'ramzinex_currencyID',
        'currency_symbol',
        'balance',
        'ramzinex_volume_precision',
        'LFAT', // Low Fund Alert Trigger
        'balance_range_lower_bound',
        'balance_range_upper_bound',
        'enable',
        'created_at',
        'updated_at',
    ];
}
