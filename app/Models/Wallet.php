<?php

namespace App\Models;

use Cassandra\Bigint;
use Illuminate\Database\Eloquent\Model;


/**
 * @property Bigint id
 * @property integer account_id
 * @property integer currency_id
 * @property string currency_symbol
 * @property double balance
 */
class Wallet extends Model
{
    protected $table = 'wallet';
    protected $fillable =
        [
            'id',
            'account_id',
            'currency_id',
            'currency_symbol',
            'balance',
            'created_at',
            'updated_at',
        ];
}
