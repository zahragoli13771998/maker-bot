<?php

namespace App\Models;

use Cassandra\Bigint;
use Illuminate\Database\Eloquent\Model;


/**
 * @property Bigint id
 * @property string name
 * @property integer account_id
 * @property integer exchange_id
 * @property string exchange_name
 * @property string bearer_token
 * @property integer ramzinex_user_id
 */
class Account extends Model
{
    protected $table = 'account';
    protected $fillable =
        [
            'id',
            'account_id',
            'name',
            'exchange_id',
            'exchange_name',
            'bearer_token',
            'created_at',
            'updated_at',
            'ramzinex_user_id'
        ];
}
