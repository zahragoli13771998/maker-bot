<?php

namespace App\Models;

use Cassandra\Bigint;
use Faker\Provider\Text;
use Illuminate\Database\Eloquent\Model;
use Nette\Utils\DateTime;
use PhpParser\Node\Scalar\String_;


/**
 * @property Bigint id
 * @property string name
 * @property int pair_id
 * @property int exchange_id_1
 * @property int exchange_id_2
 * @property double lot_size
 * @property double spread
 * @property int threshold_enable
 * @property double threshold
 * @property int time_interval
 * @property int enable
 * @property int enable_last_state
 * @property double daily_max_trade_limit
 * @property DateTime created_at
 * @property DateTime updated_at
 */
class BotConfig extends Model
{
    protected $table = 'BotConfig';
    protected $fillable = [
        'id',
        'name',
        'pair_id',
        'exchange_id_1',
        'exchange_id_2',
        'lot_size', // max vol for each order
        'spread',
        'threshold_enable',
        'threshold',
        'time_interval',
        'enable',
        'enable_last_state',
        'daily_max_trade_limit',
        'account_id',
        'created_at',
        'updated_at',
    ];
}
