<?php

namespace App\Models;

use Cassandra\Bigint;
use Illuminate\Database\Eloquent\Model;


/**
 * @property Bigint id
 * @property int currency_id
 * @property string exchange_name
 * @property int exchange_id
 * @property int symbol
 * @property int pack_num
 * @property int volume_precision
 * @property int price_precision
 * @property DateTime created_at
 * @property DateTime updated_at
 */
class ExchangeCurrency extends Model
{
    protected $fillable = [
        'id',
        'currency_id',
        'symbol',
        'pack_num',
        'exchange_name',
        'exchange_id',
        'volume_precision',
        'price_precision',
        'created_at',
        'updated_at',
    ];
    protected $table = 'ExchangeCurrency';
}
