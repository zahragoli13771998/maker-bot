<?php

namespace App\Models;

use Brick\Math\BigInteger;
use Cassandra\Bigint;
use Illuminate\Database\Eloquent\Model;
use Nette\Utils\DateTime;

/**
 * @property Bigint id
 * @property Bigint order_id
 * @property int account_id
 * @property int base_currency_id
 * @property int quote_currency_id
 * @property int base_precision
 * @property int quote_precision
 * @property double amountA
 * @property double amountB
 * @property string side
 * @property double execute_price
 * @property double bot_execute_price
 * @property double filled_percent
 * @property double filled_volume
 * @property double high_ok_price
 * @property double low_ok_price
 * @property int state
 * @property int bot_id
 * @property Bigint created_unix
 * @property DateTime created_at
 * @property DateTime updated_at
 */

class Order extends Model
{
    protected $table = 'Ramzinex_order';
    protected $fillable = [
        'id',
        'order_id',
        'account_id',
        'base_currency_id',
        'quote_currency_id',
        'base_precision',
        'quote_precision',
        'amountA',
        'amountB',
        'side',
        'execute_price',
        'bot_execute_price',
        'filled_percent',
        'filled_volume',
        'high_ok_price',
        'low_ok_price',
        'state',
        'bot_id',
        'created_unix',
        'created_at',
        'updated_at',

    ];
}
