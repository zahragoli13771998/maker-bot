<?php

namespace App\Models;

use Cassandra\Bigint;
use Illuminate\Database\Eloquent\Model;


/**
 * @property Integer id
 * @property string uuid
 * @property string order_id
 * @property string symbol
 * @property double size
 * @property double fund
 * @property double side
 * @property string type
 * @property double deal_fund
 * @property double deal_size
 * @property double fee
 * @property string fee_currency
 * @property Integer unix_created_at_kucoin
 * @property integer reference_order_id
 * @property integer state
 * @property integer bot_id
 * @property integer quote_currency_id
 * @property integer tether_change_state
 * @property integer account_id
 * @property integer residual_check_state // 0 is not check 1 is checked and every thing is ok 2 is size != deal_size
 * @property DateTime created_at
 * @property DateTime updated_at

 */
class KucoinOrder extends Model
{
    protected $table = 'KucoinOrder';
    protected $fillable = [
        'id',
        'uuid',
        'order_id',
        'symbol',
        'size',
        'fund',
        'side',
        'type',
        'deal_fund',
        'deal_size',
        'fee',
        'fee_currency',
        'unix_created_at_kucoin',
        'reference_order_id',
        'state',
        'quote_currency_id',
        'tether_change_state',
        'account_id',
        'residual_check_state',
        'created_at',
        'updated_at',
    ];
}
