<?php

namespace App\Models;

use Cassandra\Bigint;
use Illuminate\Database\Eloquent\Model;


/**
 * @property Bigint id
 * @property int robot_active
 * @property int turn_off_time
 * @property int turn_on_time
 * @property date created_at
 * @property date updated_at
 */
class Robot extends Model
{
    protected $table = 'Robot';
    protected $fillable = [
        'id',
        'robot_active',
        'turn_off_time',
        'turn_on_time',
        'created_at',
        'updated_at'
    ];
}
