<?php

namespace App\Logic;


use App\Models\Account;
use App\Models\Currency;
use App\Models\KucoinCurrency;
use App\Models\Order;
use Carbon\Carbon;
use Couchbase\LookupGetFullSpec;
use Illuminate\Support\Facades\Cache;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Monolog\Handler\LogglyHandler;
use phpDocumentor\Reflection\Types\False_;
use function Symfony\Component\Translation\t;


class MainManagerV2
{

    private static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public function getWithdrawDeposits($account, $number, $type)
    {
        if ($type == 'deposit') {
            $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/funds/deposits?offset=0&limit=' . $number;
        } else {
            $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/funds/withdraws?offset=0&limit=' . $number;
        }

        $account_id = $account->account_id;

        try {
            $account = Account::query()->where('account_id', '=', $account_id)->first();

            $bearerToken = $account->bearer_token;
        } catch (Exception $exception) {
            Log::error($exception);
            Log::info('problem in getting account token from database :getOpenOrders');
            return false;
        }

        $headers = [
            'Connection' => 'keep-alive',
            'Accept' => '*/*',
            'Cache-Control' => 'no-cache',
            'Authorization' => $bearerToken,
            'User-Agent' => 'PostmanRuntime/7.29.0',
        ];
        $result = $this->getToRamzinex($url, $headers);

//        Log::info('curl result' . $url);
//        Log::info($result);
        try {
            $result = json_decode($result, true);
            if ($result['status'] == 0 and $result['count'] > 0) {
                $orders = $result['data'];

                return $orders;
            } elseif ($result['status'] == 0 and $result['count'] == 0) {

                return [];

            } else if ($result['status'] == 1) {
                return false;
            } else {
                return false;
            }

        } catch (Exception $exception) {
            Log::info($exception);
            Log::info('exception in curl');
            return false;
        }

    }
    public function getPrices(){
        $url = 'https://publicapi.ramzinex.com/exchange/api/v1.0/exchange/pairs';

        $header = [
            'User-Agent' => 'PostmanRuntime/7.29.0',
            'Connection' => 'keep-alive',
            'Accept' => '*/*',
            'Cache-Control' => 'no-cache'
        ];

        $result = $this->getToRamzinex($url, $header);

        try {
            if ($result->status() == 200) {
                $result = json_decode($result, true);
                $data = $result['data'];

                return $data;

            } else {
                return false;
            }
        } catch (Exception $exception) {
            Log::info($exception);
            Log::info($result);
            Log::info($result->status());
            return false;
        }
    }


    public function saveDetails($ramzinex_currencyID, $exchange_volume_precision, $exchange_currency_symbol)
    {
        $check = Currency::query()->where('ramzinex_currencyID', '=', $ramzinex_currencyID)->first();
        if (!is_null($check)) {
            return 'its duplicate';
        }

        $url = 'https://publicapi.ramzinex.com/exchange/api/v1.0/exchange/currencies/' . $ramzinex_currencyID;
        $api_requests = Http::get($url)->json();

        $data = $api_requests['data'];
        $ramzinex_currencyID = $data['id'];
        $balance = 0;
        $ramzinex_volume_precision = $data['show_precision'];
        $LFAT = 0;
        $created_at = Carbon::now();
        $update_at = Carbon::now();

        try {
            $currency = new Currency();
            $currency->ramzinex_currencyID = $ramzinex_currencyID;
            $currency->currency_symbol = $exchange_currency_symbol;
            $currency->balance = $balance;
            $currency->ramzinex_volume_precision = $ramzinex_volume_precision;
            $currency->LFAT = $LFAT;
            $currency->created_at = $created_at;
            $currency->updated_at = $update_at;
            $currency->save();
            return true;
        } catch (Exception $exception) {
            return false;
        }

        // test check : its working

    }

    public function getAllPrice()
    {

        $url = 'https://publicapi.ramzinex.com/exchange/api/v1.0/exchange/pairs';

        $header = [
            'User-Agent' => 'PostmanRuntime/7.29.0',
            'Connection' => 'keep-alive',
            'Accept' => '*/*',
            'Cache-Control' => 'no-cache'
        ];

        $result = $this->getToRamzinex($url, $header);

        try {
            if ($result->status() == 200) {
                $result = json_decode($result, true);
                $data = $result['data'];

                foreach ($data as $pair) {
                    $key = 'RamzinexPrice-' . $pair['pair_id'];
                    $price = $pair['buy'];
                    Cache::put($key, $price);
                }
                return 'saved';

            } else {
                return false;
            }
        } catch (Exception $exception) {
            Log::info($exception);
            Log::info($result);
            Log::info($result->status());
            return false;
        }
        // test check : its working
    }


    public function getAllAccountsOpenOrders($number)
    {
        $open_orders = array();
        try {
            $accounts = Account::query()->get();

            foreach ($accounts as $account) {

                $open_orders[$account->account_id] = $this->getOpenOrders($account, $number);

            }
            return $open_orders;
        } catch (Exception $exception) {
            Log::info('problem in getting account token from database : getAllAccountsOpenOrders');
            Log::info($exception);
            return false;
        }
    }

    public function getAllAccountsBalanceSummary()
    {
//        Log::info('getAllAccountsBalanceSummary');
        $balances = array();
        try {
            $accounts = Account::query()->get();
//            Log::info($accounts);
            foreach ($accounts as $account) {
//                RamzinexManager::getInstance()->getAvailableBalance($account);
                $balances[$account->account_id] = $this->getBalanceSummary($account);
                if ($balances[$account->account_id] == false) {
                    return false;
                }
//                Log::info($account);
//                Log::info($this->getBalanceSummary($account));
            }


            return $balances;

        } catch (Exception $exception) {
            Log::info('problem in getting account token from database getAllAccountsBalanceSummary');
            return false;
        }

    }

    public function getPrice($pairID) ///////////////////////////////////////////////////////////////////////////
    {
        $key = 'RamzinexPrice-' . $pairID;
        try {
            if (Cache::has($key)) {
                $price = Cache::get($key);
                return $price;
            } else {
                $this->getAllPrice();
                $price = Cache::get($key);
                return $price;
            }
        } catch (Exception $exception) {
            Log::info($exception);
            return false;
        }
        // test check : its working
    }

    public function getOpenOrders($account, $number)
    {
        $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/orders2';

        $account_id = $account->account_id;

        try {
            $account = Account::query()->where('account_id', '=', $account_id)->first();

            $bearerToken = $account->bearer_token;
        } catch (Exception $exception) {
            Log::error($exception);
            Log::info('problem in getting account token from database :getOpenOrders');
            return false;
        }

        $body = ['limit' => $number, 'offset' => 0, 'states' => [1]];

        $headers = ['Authorization:' . $bearerToken, 'Content-Type:' . 'application/json'];

        $result = $this->postHttpsResponseWithHeaders($url, json_encode($body), $headers);

        try {
            if ($result['http_code'] == 200) {
                $result = json_decode($result['result'], true);
                if ($result['status'] == 0 and $result['count'] > 0) {
                    $orders = $result['data'];
//                    Cache::put('open_orders', $orders);
                    return $orders;
                } elseif ($result['status'] == 0 and $result['count'] == 0) {
//while using tradeBotManager
//                    return false;
                    //while using tradeBotManagerV2
                    return [];
                    // there is no order
                } else if ($result['status'] == 1) {
                    return false;
                } else {
                    return false;
                }
            } else {
                Log::info($result['http_code']);
                return false;
            }
        } catch (Exception $exception) {
            Log::info($exception);
            Log::info($result['http_code']);
            return false;
        }


        // test check : its working


    }

    public function getOneOrder($orderID, $bot)
    {
        $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/orders2/' . $orderID;
        $account_id = $bot->account_id;
        try {
            $account = Account::query()->where('account_id', '=', $account_id)->first();

            $bearerToken = $account->bearer_token;
        } catch (Exception $exception) {
            Log::info('problem in getting account token from database getOneOrder');
            return false;
        }


        $header = [
            'Authorization' => $bearerToken,
            'User-Agent' => 'PostmanRuntime/7.29.0',
            'Connection' => 'keep-alive',
            'Accept' => '*/*',
            'Cache-Control' => 'no-cache'
        ];

        $result = $this->getToRamzinex($url, $header);
        try {
            if ($result->status() == 200) {
                $result = json_decode($result, true);
                if ($result['status'] == 0) {
                    $orders = $result['data'];
                    return $orders;
                } else if ($result['status'] == 1) {
                    return false;
                } else {
                    return false;
                }
            } else {
                Log::info($result);
                Log::info('getOneOrder else');

                return false;
            }
        } catch (Exception $exception) {
            Log::info($exception);
            Log::info($result);
            Log::info($result->status());
            Log::info('getOneOrder');
            return false;
        }


        // test check : its working

    }

    public function setLimitOrder($pair, $amount, $price, $type, $bot, $high_ok_price, $low_ok_price)
    {
//        Log::info('setLimitOrder');
        $account_id = $bot->account_id;
        $bot_id = $bot->id;

        try {
            $account = Account::query()->where('account_id', '=', $account_id)->first();

            $bearerToken = $account->bearer_token;
        } catch (Exception $exception) {
            Log::info('problem in getting account token from database setLimitOrder');
            return false;
        }

        $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/orders/limit';

        $headers = ['Authorization:' . $bearerToken, 'Content-Type:' . 'application/json'];

        $pair_id = $pair->pair_id;
        $base_currency_id = $pair->base_currency_id;
        $quote_currency_id = $pair->quote_currency_id;

//        $body = '{"pair_id":12, "amount":0.00460, "price":12000, "type":"buy"}';

        $body = ['pair_id' => $pair_id, 'amount' => $amount, 'price' => $price, 'type' => $type];
        $body = json_encode($body);
        try {
            $result = $this->postHttpsResponseWithHeaders($url, $body, $headers);

        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

        try {
            if ($result['http_code'] == 200) {
                $result = json_decode($result['result'], true);
                if ($result['status'] == 0) {
                    try {

                        $order_id = $result['data']['order_id'];
                        $order = new Order();
                        $order->order_id = $order_id;
                        $order->base_currency_id = $base_currency_id;
                        $order->quote_currency_id = $quote_currency_id;
                        $order->base_precision = 0;
                        $order->quote_precision = 0;
                        $order->amountA = $amount;
                        $order->amountB = (double)($amount) * $price;
                        $order->side = $type;
                        $order->execute_price = $price;
                        $order->bot_execute_price = 0;
                        $order->filled_percent = 0;
                        $order->filled_volume = 0;
                        $order->high_ok_price = $high_ok_price;
                        $order->low_ok_price = $low_ok_price;
                        $order->state = 1;
                        $order->bot_id = $bot_id;
                        $order->account_id = $account_id;
                        $order->created_unix = time();
                        $order->created_at = Carbon::now();
                        $order->updated_at = Carbon::now();
                        $order->save();

                        CacheManager::getInstance()->setCacheOrderId($pair_id, $type, $order_id, $account_id);
                        return true;
                    } catch (Exception $exception) {
                        Log::info($exception);
                    }
                } else {
                    Log::info($result);
                    Log::info($pair);
                    Log::info($body);
                    return false;
                }
            } else {
                Log::info($pair);
                Log::info($body);

                return false;
            }
        } catch (Exception $exception) {
            Log::info($exception);
            Log::info($result);
            Log::info($body);


            return false;
        }

        // test check : its working

    }

    public function cancelOrder($orderID, $bot)
    {
        $account_id = $bot->account_id;
        try {
            $account = Account::query()->where('account_id', '=', $account_id)->first();

            $bearerToken = $account->bearer_token;
        } catch (Exception $exception) {
            Log::info('problem in getting account token from database');
            return false;
        }
        $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/orders/' . $orderID . '/cancel';


        $headers = ['Authorization:' . $bearerToken, 'Content-Type:' . 'application/json'];

        $body = [];


        $result = $this->postHttpsResponseWithHeaders($url, $body, $headers);

        try {
            if ($result['http_code'] == 200) {
                $result = json_decode($result['result'], true);

                if ($result['status'] == 0) {
                    return true;
                }
                if ($result['status'] == -3) {
                    return true;
                } else if ($result['status'] == 1) {
                    Log::info($orderID);
                    Log::info($result);
                    Log::info('cancelOrder 1');
                    return false;
                } else {
                    Log::info($bot);
                    Log::info($bearerToken);
                    Log::info($orderID);
                    Log::info($result);
                    Log::info('cancelOrder 2');
                    return false;
                }
            }
        } catch (Exception $exception) {
            Log::info($exception);
            Log::info($result);
            Log::info('cancelOrder 3');

            return false;
        }


        // test check : its working


    }

    public function getFunds()
    {
        $url = 'https://ramzinex.com/exchange/internal/exchange/get_fundV2';
        $header = [
//        'Authorization' => $bearerToken,
//        'User-Agent' => 'PostmanRuntime/7.29.0',
            'Connection' => 'keep-alive',
            'User-Agent' => '',
            'Accept' => '*/*',
            'Cache-Control' => 'no-cache',
        ];
        $body = ["pass" => "", "user_id" => ];
        #$result = Http::post($url,$header);
        $result = $this->postHttpsResponseWithHeaders($url, json_encode($body), $header);
//        Log::info($result);
        return 1;
    }

    public function cancelAllOrders()
    {

    }


    public function getBalanceSummary($account)
    {
//        Log::info('getBalanceSummary');
        $all_balances = [];
//        $robot_account_id = 0;
//        if ($account_id === 1696978) {
//            $robot_account_id = 2;
//        } else {
//            $robot_account_id = 1;
//        }

        $url = 'https://ramzinex.com/exchange/internal/exchange/get_fundV2';

        $header = [
//            'User-Agent'=>'PostmanRuntime/7.29.0',
            'Connection' => 'keep-alive',
            'Accept' => '*/*',
            'Cache-Control' => 'no-cache',
            'User-Agent' => '',

        ];
        $body = ["pass" => '', "user_id" => $account->ramzinex_user_id];

//        $result = $this->postHttpsResponseWithHeaders($url, json_encode($body), $header);
        $result = HTTP::withHeaders($header)->post($url, $body);
//        Log::info($result);
        try {
            if ($result != null) {
                $result = json_decode($result, true);
                foreach ($result as $id => $currency) {
                    if (Cache::has('currency_' . $id . '_precision')) {
                        $precision = Cache::get('currency_' . $id . '_precision');

                    } else {
                        $precision = RamzinexManager::getInstance()->cachePrecision($id);
                    }
                    $balance = $currency['net'] * pow(10, -($precision[0] + 2));
                    $all_balances[$id] = $balance;
                    $withdraws = $currency['withdraws']* pow(10, -($precision[0] + 2));
                    $deposits = $currency['deposits']* pow(10, -($precision[0] + 2));
                    CacheManager::getInstance()->forgetBalanceSummery(intval($id), $account->account_id);
                    CacheManager::getInstance()->setBalanceSummery(intval($id), $balance, $account->account_id);
                    CacheManager::getInstance()->forgetWithdrawDeposits(intval($id), $account->account_id);
                    CacheManager::getInstance()->setWithdrawDeposits(intval($id),$withdraws,$deposits, $account->account_id);
                }

                return $all_balances;
//                return true;
            } else {
                Log::info('result is null');
                Log::info($body);
                return false;
            }
        } catch (Exception $exception) {
            Log::info($exception);
//            Log::info($result);
            return false;
        }

        // test check : its working


    }

// added to handel trade botManagerv2
    public function getBalanceSummaryV3($account)
    {

        $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/funds/summaryDesktop';

        $account_id = $account->account_id;
//        Log::info("account id: ".$account_id);
        try {
            $account = Account::query()->where('account_id', '=', $account_id)->first();

            $bearerToken = $account->bearer_token;
        } catch (Exception $exception) {
            Log::info('problem in getting account token from database-getBalanceSummary');
            return false;
        }

        $header = [
            'Authorization' => $bearerToken,
            'User-Agent' => 'PostmanRuntime/7.29.0',
            'Connection' => 'keep-alive',
            'Accept' => '*/*',
            'Cache-Control' => 'no-cache'
        ];

        $result = $this->getToRamzinex($url, $header);

        for ($i = 0; $i < 4 && $result->status() != 200; $i++) {
            $result = $this->getToRamzinex($url, $header);
            sleep(2);
        }

        try {
            for ($i = 0; $i < 4 && $result->status() != 200; $i++) {
                $result = $this->getToRamzinex($url, $header);
                sleep(2);
            }

            if ($result->status() == 200) {
                $result = json_decode($result, true);
                foreach ($result['data'] as $currency) {
                    $currency_id = $currency['currency_id'];
                    $balance = $currency['total_nr'];

                    CacheManager::getInstance()->setBalanceSummery($currency_id, $balance, $account_id);
                }
                return $result['data'];
            } else {
                return false;
            }
        } catch (Exception $exception) {
            Log::info($exception);
            Log::info('curl error in getBalanceSummaryV2' . $url);

//            Log::info($result);
            return false;
        }
    }

    public function getBalanceSummaryV2($account)
    {
        Log::info('getBalanceSummary');
        $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/funds/summaryDesktop';

        $account_id = $account->account_id;
        try {
            $account = Account::query()->where('account_id', '=', $account_id)->first();

            $bearerToken = $account->bearer_token;
        } catch (Exception $exception) {
            Log::info('problem in getting account token from database');
            return false;
        }

        $header = [
            'Authorization' => $bearerToken,
            'User-Agent' => 'PostmanRuntime/7.29.0',
            'Connection' => 'keep-alive',
            'Accept' => '*/*',
            'Cache-Control' => 'no-cache',
           'User-Agent' => ''
        ];

        $result = $this->getToRamzinex($url, $header);
        try {

            if ($result->status() == 200) {
                $result = json_decode($result, true);
                foreach ($result['data'] as $currency) {
                    $currency_id = $currency['currency_id'];
                    $balance = $currency['total_nr'];
                    CacheManager::getInstance()->forgetBalanceSummery($currency_id, $account_id);
                    CacheManager::getInstance()->setBalanceSummery($currency_id, $balance, $account_id);
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception $exception) {
            Log::info($exception);
            Log::info($result->status());
//            Log::info($result);
            Log::info('curl error in getBalanceSummary');
            return false;
        }

     


    }
    public function getAllOneBalance($currencyID, $bot)
    {
        $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/funds/total/currency/' . $currencyID . '/';

        $account_id = $bot->account_id;
        try {
            $account = Account::query()->where('account_id', '=', $account_id)->first();

            $bearerToken = $account->bearer_token;
        } catch (Exception $exception) {
            Log::info('problem in getting account token from database');
            return false;
        }


        $header = [
            'Authorization' => $bearerToken,
            'User-Agent' => 'PostmanRuntime/7.29.0',
            'Connection' => 'keep-alive',
            'Accept' => '*/*',
            'Cache-Control' => 'no-cache'
        ];

        $result = $this->getToRamzinex($url, $header);

        try {
            if ($result->status() == 200) {
                $result = json_decode($result, true);
                if ($result['status'] == 0) {
                    $balance = $result['data'];
                    return $balance;
                } else if ($result['status'] == 1) {
                    return false;
                } else {
                    return false;
                }
            } else {
//                Log::info($result);
                Log::info('curl error getToRamzinex (1)');
                return false;
            }

        } catch (Exception $exception) {
            Log::info($exception);
            Log::info($result->status());
            Log::info('curl error getToRamzinex (2)');

//            Log::info($result);
            return false;
        }
        // test check : its working

    }

    public function getAvailableBalance($account)
    {
        //mock
//        return true;

        $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/funds/refresh';

        try {
            $bearerToken = $account->bearer_token;
        } catch (Exception $exception) {
            Log::info('problem in getting account token from database');
            return false;
        }
        $body = [' ' => ' '];

        $headers = ['Authorization:' . $bearerToken, 'Content-Type:' . 'application/json',
           , 'User-Agent' => ''
        ];
        $result = $this->postHttpsResponseWithHeaders($url, json_encode($body), $headers);

        try {
            if ($result['http_code'] == 200) {
                $result = json_decode($result['result'], true);
                if ($result['status'] == 0) {
                    return true;
                } else if ($result['status'] == 1) {
                    return false;
                } else {
                    return false;
                }
            } else {
                Log::info($result['status']);
                Log::info('curl error in getAvailableBalance (1)' . $url);
//                Log::info($result);
                return false;
            }
        } catch (Exception $exception) {
            Log::info($exception);
            Log::info('curl error in getAvailableBalance (2)' . $url);

//            Log::info($result);
            return false;
        }

    }

    public function updateBalance($currency_id, $bot)
    {
        $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/funds/available/currency/' . $currency_id . '/';

        $account_id = $bot->account_id;
        try {
            $account = Account::query()->where('account_id', '=', $account_id)->first();

            $bearerToken = $account->bearer_token;
        } catch (Exception $exception) {
            Log::info('problem in getting account token from database');
            return false;
        }

        $header = [
            'Authorization' => $bearerToken,
            'User-Agent' => 'PostmanRuntime/7.29.0',
            'Connection' => 'keep-alive',
            'Accept' => '*/*',
            'Cache-Control' => 'no-cache'
        ];

        $result = $this->getToRamzinex($url, $header);

        try {
            if ($result->status() == 200) {
                $result = json_decode($result, true);

                if ($result['status'] == 0) {
                    $balance = $result['data'];
                    return $balance;
                } else if ($result['status'] == 1) {
                    return false;
                } else {
                    return false;
                }
            } else {
                Log::info($result);
                Log::info($result->status());
                return false;
            }
        } catch (Exception $exception) {
            Log::info($exception);
            Log::info($result);
            Log::info($result->status());
            return false;
        }


    }

    function bubbleSort(&$arr1, $ascDsc)
    {
        $asc = 1;
        if ($ascDsc == 'dsc')
            $asc = -1;

        $n = sizeof($arr1);
        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $n - $i - 1; $j++) {
//                if ($arr1[$j]['price'] > $arr1[$j+1]['price'])
                if ($asc * ($arr1[$j]['price'] - $arr1[$j + 1]['price']) > 0) {
                    $t = $arr1[$j];
                    $arr1[$j] = $arr1[$j + 1];
                    $arr1[$j + 1] = $t;
                }
            }
        }
    }

    public function cancelOnePairOrders($pair_id, $bot) ////////////////////////////////////////////////////////////////////////
    {

        $account_id = $bot->account_id;
        try {
            $account = Account::query()->where('account_id', '=', $account_id)->first();

            $bearerToken = $account->bearer_token;
        } catch (Exception $exception) {
            Log::info('problem in getting account token from database');
            return false;
        }

        $url = 'https://ramzinex.com/exchange/api/v1.0/exchange/users/me/cancelAllOpenOrders';
        $headers = ['Authorization:' . $bearerToken, 'Content-Type:' . 'application/json'];
        $body = ['pair_id' => $pair_id];

        $this->postHttpsResponseWithHeaders($url, $body, $headers);
    }

    public function cacheUsdtIrrOrderbook()
    {
//        Log::info('cacheUsdtIrrOrderbook***');

        try {
            $response_data = Http::get('https://publicapi.ramzinex.com/exchange/api/v1.0/exchange/orderbooks/11/buys_sells')->json();
            $sells = [];
            $buys = [];

            foreach ($response_data['data']['buys'] as $order) {
                $buys[] = ["price" => $order[0], "size" => $order[1], "value" => $order[2]];
            }
            foreach ($response_data['data']['sells'] as $order) {
                $sells[] = ["price" => $order[0], "size" => $order[1], "value" => $order[2]];
            }

            $this->bubbleSort($buys, 'dsc');
            $this->bubbleSort($sells, 'asc');

            Cache::put('theter_orderbook', ['buys' => $buys, 'sells' => $sells]);

        } catch (Exception $e) {
            Log::info($e);
        }
        //in old version there is no need to return some thing
        return 1;


    }

    public function getLastExecuted($pair_id)
    {

        $url = "https://publicapi.ramzinex.com/exchange/api/v1.0/exchange/orderbooks/" . $pair_id . "/trades";

        $header = [
            'User-Agent' => 'PostmanRuntime/7.29.0',
            'Connection' => 'keep-alive',
            'Accept' => '*/*',
            'Cache-Control' => 'no-cache'
        ];

        $result = $this->getToRamzinex($url, $header);

        try {
            if ($result->status() == 200) {
                $result = json_decode($result, true);
                $data = $result['data'];
                return $data;

            } else {
                return false;
            }
        } catch (Exception $exception) {
            Log::info($exception);
            Log::info($result);
            Log::info($result->status());
            return false;
        }
    }

    public function getImmediateLimitOrderPrice(string $side, int $pair_id)
    {
        $rangeSize = 1500;

        $lastPrice = RamzinexManager::getInstance()->getLastExecuted($pair_id)[0][0];
        try {

            if ($side == "buy") {
                return $lastPrice + $rangeSize;

            } elseif ($side == "sell") {
                return $lastPrice - $rangeSize;
            } else {
                return -1;
            }
        } catch (Exception $e) {
            Log::info($e);
            return -1;
        }
    }

    public function sendRequestToRamzinex($url, $data, $headers, $body)
    {

        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER => false,            // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING => "",             // handle all encodings
            CURLOPT_USERAGENT => "",            // who am i
            CURLOPT_AUTOREFERER => true,        // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT => 120,             // timeout on response
            CURLOPT_MAXREDIRS => 10,            // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
        );


        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        if ($headers) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        return $content;
    }

    public function postRequestToRamzinex($url, $header, $body)
    {
        $id = Str::uuid();

        try {
            $result = HTTP::withHeaders($header)->post($url, $body);

            return $result;
        } catch (Exception $e) {
            Log::info($e);
        }
    }

    public function getToRamzinex($url, $header)
    {
        $res = Http::withHeaders($header)->get($url);

        return $res;
    }

    public function postHttpsResponseWithHeaders($url, $postfields, $headers)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_STDERR, fopen('./test.log', 'w'));

        $result = curl_exec($ch);


        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $data = [
            "result" => $result,
            "http_code" => $httpCode
        ];
        return $data;
    }
}
