<?php

namespace App\Logic;

use App\Models\BotConfig;
use App\Models\Pair;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class ChangeCurrencyManager
{
    private static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {

        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public function changeCurrency($dif, $currency_id)
    {
        if ($dif > 0) {
            $side = 'sell';
        } else {
            $side = 'buy';
        }

        $pair = $this->findPairForCurrency($currency_id);

        $size = OrderManager::getInstance()->applyVolumePrecision(2, abs($dif), $pair);
        traderBotManager::getInstance()->tradeSecondaryExchangeV2($side, $pair, $size, 2);
        Log::info("TRADE KUCOIN");
        Log::info($side);
        Log::info($pair);
        Log::info($size);
        Log::info(abs($dif));

    }

    public function findPairForCurrency($currency_id)
    {
        return Pair::query()->where("base_currency_id", "=", $currency_id)->first();

    }

    public function checkCurrenciesForChange($dif)
    {
        $dif_values = $this->calculateDifValue($dif);

        foreach ($dif_values as $currency_id => $dif_value) {
            if ($dif_value >1000000){
                Log::info($currency_id.'-----value:'.$dif_value.'--------diff:'.$dif[$currency_id]);
            }
            if ($this->ifChangeNeeded($dif_value, $currency_id)) {
                $this->changeCurrency($dif[$currency_id], $currency_id);
            }
        }
    }

    public function ifChangeNeeded($dif_value, $currency_id)
    {
        if ($currency_id == 2 || $currency_id == 9) {
            return false;
        }

        if ($dif_value > 1000000 && $dif_value < 1000000000) {
            return true;
        } elseif ($dif_value > 300000000) {
            Log::info("dif value is more than 300000000=>" . $currency_id . '-----' . $dif_value);
        }
        return false;

    }

    public function changeTether($dif)
    {
        $pair = tradeBotManagerV2::getInstance()->getPairById(11);
        $bot = BotConfig::query()->where('pair_id', '=', 11)->where('account_id', '=', 2)->first();
        $dif_value = abs($dif);
        if ($dif_value > 2000000 && $dif_value < 1500000000) {
            if ($dif < 0) {
                $side = 'sell';
            } else {
                $side = 'buy';
            }
            $price = MainManagerV2::getInstance()->getImmediateLimitOrderPrice($side, 11);
            $total_size = $dif_value / $price;
            MainManagerV2::getInstance()->setLimitOrder($pair, $total_size, $price, $side, $bot, 0, 0);
        } elseif ($dif_value > 1000000000) {
            Log::info("changeTether => dif value is more than 1000000000" . $dif);
        }


    }

    public function calculateDifValue($difs)
    {
        $prices = MainManagerV2::getInstance()->getPrices();
        $currency_prices = $this->getCurrencyPrices($prices);

        $dif_values = [];
        foreach ($difs as $currency_id => $dif) {
            if ($currency_id == 2) {
                $dif_values[$currency_id] = abs($dif);
            } else {
                $dif_values[$currency_id] = abs($dif * $currency_prices[$currency_id]);

            }

        }
        return $dif_values;


    }

    public function getCurrencyPrices($prices)
    {
        $currency_prices = [];
        foreach ($prices as $price) {
//            'base_currency_id' => 203,
//            'quote_currency_id' => 2,
            if ($price['quote_currency_id'] == 2) {
                $currency_prices[$price['base_currency_id']] = floatval($price['sell']) / CacheManager::getInstance()->getPackNum($price['base_currency_id'], 1);
            }

        }
        return $currency_prices;

    }

//to save checkpoints for the first time
    public function saveCheckPoints()
    {
        //get each account's withdraw/deposits
        $withdraw_deposits = [];
        $accounts = tradeBotManagerV2::getInstance()->getAccounts();
        foreach ($accounts as $account) {
            $withdraw_deposits[$account->account_id] = CacheManager::getInstance()->getWithdrawDeposits($account->account_id);
        }

        //get all accounts balances
        $balances = $this->sumRmzxBalances();

        //calculate the diff

        $this->calculateDiff($balances, $withdraw_deposits);
        //save the diff as check points
    }

//to save checkpoints for the first time
    public function calculateDiff($balances, $net_withdraw_deposits)
    {
        $currencies = keepBalanceSteady::getInstance()->getCurrencies();
        $diff = [];
        $sum_diff = [];
//        Log::info('calculate the diff');

        foreach ($currencies as $currency) {
            $sum_net = floatval($net_withdraw_deposits[1][$currency->currencyID]) + floatval($net_withdraw_deposits[2][$currency->currencyID]);


            $sum_diff[$currency->currencyID] = $balances[$currency->currencyID] - $sum_net;
            $sum_diff[$currency->currencyID] *= CacheManager::getInstance()->getPackNum($currency->currencyID, 1);

        }
        keepBalanceSteady::getInstance()->saveCheckPoints($sum_diff, time() * 1000, 'main');

//        Log::info($sum_diff);
    }

//to save checkpoints for the first time
    public function saveKucoinCheckPoint()
    {
        $currency_ids = keepBalanceSteady::getInstance()->getCurrencies();
        $currency_balance = [];
        foreach ($currency_ids as $currency) {
            $currency_balance[$currency->main_currencyID] = Cache::get('account-kucoin-currency:' . $currency->main_currencyID . '-balance');

        }
//        Log::info($currency_balance);
        keepBalanceSteady::getInstance()->saveCheckPoints($currency_balance, time() * 1000, 'kucoin');

    }


    public function sumBalances()
    {
        $currency_ids = keepBalanceSteady::getInstance()->getCurrencies();
        $currency_balance = [];
        foreach ($currency_ids as $currency) {
            if (Cache::has('main_balance_check' . $currency->main_currencyID . '_1') && Cache::has('main_balance_check' . $currency->main_currencyID . '_2') && Cache::has('account-kucoin-currency:' . $currency->main_currencyID . '-balance')){

                $currency_balance[$currency->main_currencyID] = Cache::get('main_balance_check' . $currency->main_currencyID . '_1') * CacheManager::getInstance()->getPackNum($currency->main_currencyID, 1);
                $currency_balance[$currency->main_currencyID] += Cache::get('main_balance_check' . $currency->main_currencyID . '_2') * CacheManager::getInstance()->getPackNum($currency->main_currencyID, 1);
                $currency_balance[$currency->main_currencyID] += Cache::get('account-kucoin-currency:' . $currency->main_currencyID . '-balance');
            }

        }

        return $currency_balance;
    }

    public function sumRmzxBalances()
    {
        $currency_ids = keepBalanceSteady::getInstance()->getCurrencies();
        $currency_balance = [];

        foreach ($currency_ids as $currency) {
            $currency_balance[$currency->main_currencyID] = Cache::get('main_balance_check' . $currency->main_currencyID . '_1');
            $currency_balance[$currency->main_currencyID] += Cache::get('main_balance_check' . $currency->main_currencyID . '_2');
        }


        return $currency_balance;
    }


}
