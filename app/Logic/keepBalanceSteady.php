<?php

namespace App\Logic;

use App\Models\Account;
use App\Models\Currency;
use App\Models\WithdrawDepositsCheckpoints;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class keepBalanceSteady

{
    private static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public function updateCheckPoints()
    {

        $start = keepBalanceSteady::getInstance()->getStartFromCheckPoint();

        $withdraw_deposits = keepBalanceSteady::getInstance()->getAllAccountsWithdrawDeposits(1000, $start);

        if ($withdraw_deposits == false) {
            return false;
        }
        $kucoin_withdraw_deposits = keepBalanceSteady::getInstance()->sumWithdrawDeposit($withdraw_deposits, $start);

        $max_created_at = $start;
        foreach ($withdraw_deposits as $account => $withdraw_deposit) {
            if ($account == 'kucoin') {
                foreach ($withdraw_deposit['deposit'] as $kucoin_deposit) {
                    if ($kucoin_deposit['createdAt'] > $max_created_at) {
                        $max_created_at = $kucoin_deposit['createdAt'];
                    }
                }
                foreach ($withdraw_deposit['withdraw'] as $kucoin_withdraw) {
                    if ($kucoin_withdraw['createdAt'] > $max_created_at) {
                        $max_created_at = $kucoin_withdraw['createdAt'];
                    }
                }
            }
        }

        if ($start == 0) {
            keepBalanceSteady::getInstance()->saveCheckPoints($kucoin_withdraw_deposits, $max_created_at, 'kucoin');
        } else {
            $check_points = keepBalanceSteady::getInstance()->updateWithdrawDeposits($kucoin_withdraw_deposits);
            keepBalanceSteady::getInstance()->saveCheckPoints($check_points, $max_created_at, 'kucoin');
        }
        return true;

    }

    public function getCheckPoints()
    {
        return WithdrawDepositsCheckpoints::query()->get();
    }

    public function updateWithdrawDeposits($sum_withdraw_deposit)
    {
        $checkPoints = $this->getCheckPoints();
        if ($checkPoints != null) {
            $new_check_point = [];
            foreach ($checkPoints as $checkPoint) {
                if ($checkPoint->account_id == 'kucoin') {
                    $new_check_point[$checkPoint->currency_id] = $sum_withdraw_deposit[$checkPoint->currency_id] + $checkPoint->amount;
                }
            }
            return $new_check_point;
        }
    }

    public function saveCheckPoints($sum_withdraw_deposits, $time, $account_id)
    {

        foreach ($sum_withdraw_deposits as $currency_id => $sum_withdraw_deposit) {
            try {
                WithdrawDepositsCheckpoints::updateOrCreate(['currency_id' => $currency_id, 'account_id' => $account_id],
                    [
                        'amount' => floatval($sum_withdraw_deposit),
                        'currency_id' => $currency_id,
                        'time' => $time,
                        'account_id' => $account_id
                    ]);

            } catch (\Exception|QueryException $exception) {
                Log::error('error while saving checkpoint' . $currency_id . $exception->getMessage());
            }
        }

    }

    public function saveBalanceAsCheckPoints()
    {
        $currencies = keepBalanceSteady::getInstance()->getCurrencies();
        $currency_balance = [];

        foreach ($currencies as $currency) {
            $currency_balance[$currency->main_currencyID] = Cache::get('main_balance_check' . $currency->main_currencyID . '_1');
           $currency_balance[$currency->main_currencyID] += Cache::get('main_balance_check' . $currency->main_currencyID . '_2');
            $currency_balance[$currency->main_currencyID] += Cache::get('account-kucoin-currency:' . $currency->main_currencyID . '-balance');
        }

        foreach ($currency_balance as $currency_id => $balance) {
            try {
                WithdrawDepositsCheckpoints::updateOrCreate(['currency_id' => $currency_id],
                    [
                        'amount' => $balance,
                        'currency_id' => $currency_id,
                        'time' => time() * 1000
                    ]);

            } catch (\Exception|QueryException $exception) {
                Log::error('error while saving balance as checkpoint' . $currency_id . $exception->getMessage());
            }
        }
    }

    public function getStartFromCheckPoint()
    {
        $time = WithdrawDepositsCheckpoints::query()->pluck('time');
        try {
            if ($time == null) {
                return 0;
            }
            return $time[0];
        } catch (\Exception|QueryException $exception) {
            Log::error('problem in getting time');
            return 0;
        }

    }


    public function getAllAccountsWithdrawDeposits($number, $start)
    {
        $withdraw_deposits = array();
        try {
            $withdraw_deposits['kucoin'] = [];
            $withdraw_deposits['kucoin']['withdraw'] = KucoinManager::getInstance()->getWithdrawDeposits('withdraw', $start);
            $withdraw_deposits['kucoin']['deposit'] = KucoinManager::getInstance()->getWithdrawDeposits('deposit', $start);
            return $withdraw_deposits;
        } catch (Exception $exception) {
            Log::info('getAllAccountsWithdrawDeposits');
            Log::info($exception);
            return false;
        }
    }


    public function getCurrencies()
    {
        return Currency::query()->get();
    }

    /**
     * @param $withdraw_deposits
     * @param $start
     * @return array
     */
    public function checkIfWithdrawDepositsAreUpToDate($withdraw_deposits, $start)
    {
        Log::info('checkIfWithdrawDepositsAreUpToDate');
        $withdraw_deposits_up_to_date = [];
        foreach ($withdraw_deposits as $withdraw_deposit) {
            if ($withdraw_deposit['created_at_ms'] * 1000 > $start) {
                $withdraw_deposits_up_to_date[] = $withdraw_deposit;
            }
        }
        return $withdraw_deposits_up_to_date;
    }

    public function findCurrencyByName($currencies, $currency_name)
    {
        foreach ($currencies as $currency) {
            if ($currency->currency_symbol == $currency_name) {
                return $currency;
            }
        }
        return false;

    }


    public function sumWithdrawDeposit($withdraw_deposits, $start)
    {

        $sum_withdraw_deposit = [];
        $currencies = $this->getCurrencies();
        foreach ($currencies as $currency) {
            $sum_withdraw_deposit[$currency->main_currencyID] = 0;
        }
        foreach ($withdraw_deposits as $account_id => $account_withdraw_deposit) {
            if ($account_id == 'kucoin') {
                $withdraws = $this->checkIfKucoinDataIsUpToDate($account_withdraw_deposit['withdraw'], $start);
                $deposits = $this->checkIfKucoinDataIsUpToDate($account_withdraw_deposit['deposit'], $start);
                foreach ($withdraws as $withdraw) {
                    if ($withdraw['status'] == 'SUCCESS' and $withdraw['isInner'] == false) {
                        $currency_row = $this->findCurrencyByName($currencies, $withdraw['currency']);
                        if ($currency_row != false) {
                            $sum_withdraw_deposit[$currency_row->main_currencyID] -= $withdraw['amount'];
                        }
                    }
                }
                foreach ($deposits as $deposit) {
                    if ($deposit['status'] == 'SUCCESS' and $deposit['isInner'] == false) {
                        $currency_row = $this->findCurrencyByName($currencies, $deposit['currency']);
                        if ($currency_row != false) {
                            $sum_withdraw_deposit[$currency_row->main_currencyID] += $deposit['amount'];


                        }
                    }
                }
            }

        }

        return $sum_withdraw_deposit;
    }

    public function checkIfKucoinDataIsUpToDate($withdraw_deposits, $start)
    {
        $withdraw_deposits_up_to_date = [];
        foreach ($withdraw_deposits as $withdraw_deposit) {
            if ($withdraw_deposit['createdAt'] > $start) {
                $withdraw_deposits_up_to_date[] = $withdraw_deposit;
            }
        }
        return $withdraw_deposits_up_to_date;
    }

    public function balanceAndTransactionsDif($balances, $checkpoints)
    {
        $dif = [];
        $sum_withdraw_deposit = [];
        $currencies = $this->getCurrencies();
        foreach ($currencies as $currency) {
//            try {
            $sum_withdraw_deposit[$currency->main_currencyID] = floatval(Cache::get('net_withdraw_deposit_' . $currency->main_currencyID . '_account_1')) + floatval(Cache::get('net_withdraw_deposit_' . $currency->main_currencyID . '_account_2'));
            $sum_withdraw_deposit[$currency->main_currencyID] *= CacheManager::getInstance()->getPackNum($currency->main_currencyID, 1);
            if (array_key_exists($currency->main_currencyID, $balances)) {
                $dif[$currency->main_currencyID] = floatval($balances[$currency->main_currencyID]) - $sum_withdraw_deposit[$currency->main_currencyID];
            }

        }

        foreach ($checkpoints as $checkpoint) {
            if (array_key_exists($checkpoint->currency_id, $dif)) {
                $dif[$checkpoint->currency_id] -= floatval($checkpoint->amount);
                Cache::put('diff_amount_for_currency:'.$checkpoint->currency_id,$dif[$checkpoint->currency_id],100);
            } 

        }
        return $dif;

    }


}

