<?php

namespace App\Logic;

use App\Jobs\tradesJobV2;
use App\Models\Account;
use App\Models\BotConfig;
use App\Models\Currency;
use App\Models\ExchangeCurrency;
use App\Models\Pair;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use function PHPUnit\Framework\isEmpty;

class tradeBotManagerV2
{
    private static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * dispatch manageBotPerPair for each pair and side
     * @return int|void
     */
    public function manageBots()
    {
//        Log::info('check manageBots');
        $bot_configs = $this->getBotConfigs(1);
        if ($bot_configs) {
            foreach ($bot_configs as $bot_config) {
                $pair = $this->getPairById($bot_config->pair_id);
//                Log::info('check manageBots ' . $pair);
                if ($pair) {
//                    Log::info('check manageBots inside if ');
                    tradesJobV2::dispatch($pair, $bot_config, 'sell')->onQueue('test');
                    tradesJobV2::dispatch($pair, $bot_config, 'buy')->onQueue('test');
                } else {
                    Log::info('error in getting pair in manageBots ');
                    return -1;
                }
            }
        } else {
            Log::info('error in getting bot_configs in manageBots ');
            return -1;
        }
    }

    /**
     * set order in main if needed
     * @param  $pair
     * @param  $bot_config
     * @param string $side
     * @return int|void
     */
    public function manageBotPerPair($pair, $bot_config, string $side)
    {
//        Log::info('check manageBotPerPair');
        $account_id = $bot_config->account_id;
        $detailed_side = 'buy' == $side ? OrderManager::buy_main_sell_exchange : OrderManager::sell_main_buy_exchange;

        $price = OrderManager::getInstance()->getLogicalPriceRange($detailed_side, $pair, $bot_config);
        $price_with_precision = OrderManager::getInstance()->applyPricePrecision($bot_config, $price['main_order_price'], $pair, $side);
        $lot_size = $this->calculateLotSize($bot_config, $pair);

        $should_set_order = $this->shouldSetOrder($side, $pair, $bot_config, $account_id, $price_with_precision);
        if ($should_set_order) {

            $this->tradeMakerExchange($pair, $lot_size, $price_with_precision, $side, 1, $bot_config, $price['exchange_price_up_cancel_trigger'], $price['exchange_price_low_cancel_trigger']);
        }

    }


    /**
     * check if setting order is possible or not
     * @param string $side
     * @param  $pair
     * @param  $bot_config
     * @param $account_id
     * @param $price
     * @return bool
     */
    public function shouldSetOrder(string $side, $pair, $bot_config, $account_id, $price)
    {
        $opposite_side = 'buy' == $side ? 'sell' : 'buy';
        $open_orders = $this->getPairOpenOrders($pair->pair_id, $side, $account_id);
//        Log::info('shouldSetOrder');

        if ($open_orders == -1) {
            return false;
        } elseif ($open_orders == []) {
            $has_balance = $this->hasSufficientBalance2($side, $account_id, $bot_config, $pair);
            if (!$has_balance) {
                Log::info('shouldSetOrder : balance is not sufficient ');
                return false;
            }

            $opposite_side_open_orders = $this->getPairOpenOrders($pair->pair_id, $opposite_side, $account_id);
            if ($opposite_side_open_orders == -1) {
                Log::info('shouldSetOrder : problem in getting order ');
                return false;
            }
            $has_conflict = $this->hasPriceConflict($opposite_side_open_orders, $side, $pair, $price);
            if ($has_conflict) {
                Log::info('shouldSetOrder : conflicts occurred ');
                return false;
            }
            $is_diff_more_than_usual = $this->isDiffMoreThanUsua($pair->pair_id);
            if ($is_diff_more_than_usual) {
                Log::info('shouldSetOrder : diff more than usual ');
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public function isDiffMoreThanUsua($pair_id)
    {
        $currency_id = Pair::query()->where('pair_id', '=', $pair_id)->pluck('base_currency_id')[0];
       $bot = $this->getBotConfigByPairId($pair_id);
       $lot_size = $bot->lot_size;
        if (Cache::has('diff_amount_for_currency:' . $currency_id)) {
            $diff = Cache::get('diff_amount_for_currency:' . $currency_id);
            if (abs($diff) > 3 *$lot_size ){
                return true;
            }
        }
        return false;
    }

    /**
     * set order
     * @param $pair
     * @param $lot_size
     * @param $price
     * @param $side
     * @param $exchange_id
     * @param $bot
     * @return void
     */
    public function tradeMakerExchange($pair, $lot_size, $price, $side, $exchange_id, $bot, $high_ok_price, $low_ok_price)
    {
//        Log::info('tradeMakerExchange');

        $account_id = $bot->account_id;
        if ($exchange_id == 1) {
            MainManagerV2::getInstance()->setLimitOrder($pair, $lot_size, $price, $side, $bot, $high_ok_price, $low_ok_price);
        }
        $pair_id = $pair->pair_id;

        try {
            CacheManager::getInstance()->setOrderPrice($pair_id, $side, $price, $account_id);

        } catch (Exception $exception) {
            Log::info('error in tradeMakerExchange' . $pair_id . '=>' . $exception->getMessage());
        }

    }

    /**
     * check if we have Price Conflicts
     * @param $opposite_side_open_orders
     * @param string $side
     * @param  $pair
     * @param $price
     * @return bool
     */
    public function hasPriceConflict($opposite_side_open_orders, string $side, $pair, $price)
    {
//        Log::info('hasPriceConflict');
        foreach ($opposite_side_open_orders as $opposite_side_open_order) {
            if ($side == 'sell') {
                if ($price <= $opposite_side_open_order['order_price']) {
                    Log::info('error => manageBotPerPair of pair ' . $pair->pair_id . ' in sell: price is ' . $price . ' and opposite price is ' . $opposite_side_open_order['order_price']);
                    return true;
                }
            } elseif ($side == 'buy') {
                if ($price >= $opposite_side_open_order['order_price']) {
                    Log::info('error => manageBotPerPair of pair ' . $pair->pair_id . ' in buy: price is ' . $price . ' and opposite price is ' . $opposite_side_open_order['order_price']);
                    return true;
                }
            } else {
                Log::info('side is not sell or buy in manageBotPerPair');
                return true;
            }
        }
        return false;
    }

    /**
     * @param  $bot_config
     * @param  $pair
     * @return float|int
     */
    public function calculateLotSize($bot_config, $pair)
    {
//        Log::info('calculateLotSize');
        $pack_num = $this->getPackNum($pair);
        $lot_size = $bot_config->lot_size / $pack_num;
        $lot_size *= rand(80, 120) / 100;
        $lot_size = OrderManager::getInstance()->applyVolumePrecision(1, $lot_size, $pair);
        return $lot_size;
    }

    /**
     * @param string $side
     * @param int $account_id
     * @param  $bot_config
     * @param  $pair
     * @return bool
     */
    public function hasSufficientBalance(string $side, int $account_id, $bot_config, $pair)
    {
//        Log::info('hasSufficientBalance');
        $kucoin_price = KucoinManager::getInstance()->getPrice($pair);

        if (!$kucoin_price) {
            return false;
        }

        $base_currency_id = $pair->base_currency_id;
        $quote_currency_id = $pair->quote_currency_id;

        $base_balance = $this->getCurrencyBalances($base_currency_id, $account_id);
        $irr_balance = $this->getCurrencyBalances(2, $account_id);
        $usdt_balance = $this->getCurrencyBalances(9, $account_id);
        $lot_size = $bot_config->lot_size;
        $pack_num = $this->getPackNum($pair);


        if ($side == 'sell') {
            if ($base_balance[$account_id] * $pack_num < 2 * $lot_size) {
                return false;
            }
            // is usdt balance in kucoin is sufficient or not
            if ($usdt_balance['kucoin'] < 10 * $lot_size * $kucoin_price) {
                return false;
            }
        } elseif ($side == 'buy') {
            //is base balance in kucoin is sufficient or not
            if ($base_balance['kucoin'] < 2 * $lot_size) {
                return false;
            }
            if ($quote_currency_id == 2) {
                $usdt_price = OrderManager::getInstance()->getTheterPrice(OrderManager::buy_main_sell_exchange, 5000);
                if ($irr_balance[$account_id] < 10 * $lot_size * $kucoin_price * $usdt_price) {
                    return false;
                }
            } elseif ($quote_currency_id == 9) {
                if ($usdt_balance[$account_id] < 10 * $lot_size * $kucoin_price) {
                    return false;
                }
            } else {
                Log::info('quote_currency_id is not irr or usdt =>' . $quote_currency_id);
                return false;
            }

        } else {
            Log::info('side is not buy or sell =>' . $side);
            return false;
        }
        return true;
    }

    /**
     * @param string $side
     * @param int $account_id
     * @param  $bot_config
     * @param  $pair
     * @return bool
     */
    public function hasSufficientBalance2(string $side, int $account_id, $bot_config, $pair)
    {
//        Log::info('hasSufficientBalance');
        $kucoin_price = KucoinManager::getInstance()->getPrice($pair);
        if (!$kucoin_price) {
            return false;
        }

        $base_currency_id = $pair->base_currency_id;
        $quote_currency_id = $pair->quote_currency_id;

        $base_balance_account = $this->getCurrencyBalances($base_currency_id, $account_id);
        $base_balance_kucoin = $this->getCurrencyBalances($base_currency_id, 'kucoin');
        $irr_balance = $this->getCurrencyBalances(2, $account_id);
        $usdt_balance_account = $this->getCurrencyBalances(9, $account_id);
        $usdt_balance_kucoin = $this->getCurrencyBalances(9, 'kucoin');
        $lot_size = $bot_config->lot_size;
        $pack_num = $this->getPackNum($pair);
//        Log::info($side . 'balances=>');
//        Log::info($base_balance_kucoin);
//        Log::info($base_balance_account);
//        Log::info($usdt_balance_account);
//        Log::info($usdt_balance_kucoin);


        if ($side == 'sell') {
            // is base balance in main is sufficient or not
            if ($base_balance_account * $pack_num < 2 * $lot_size) {
                Log::info('base balance in main is not sufficient *** pair_id='.$pair->pair_id);
                return false;
            }
            // is usdt balance in kucoin is sufficient or not
            if ($usdt_balance_kucoin < 10 * $lot_size * $kucoin_price) {
                Log::info('usdt balance in kucoin is not sufficient *** pair_id='.$pair->pair_id);
                return false;
            }
        } elseif ($side == 'buy') {
            //is base balance in kucoin is sufficient or not
            if ($base_balance_kucoin < 2 * $lot_size) {
                Log::info('base balance in kucoin is not sufficient *** pair_id='.$pair->pair_id);
                return false;
            }
            //is quote balance in main is sufficient or not
            if ($quote_currency_id == 2) {
                $usdt_price = OrderManager::getInstance()->getTheterPrice(OrderManager::buy_main_sell_exchange, 5000);
                if ($irr_balance < 10 * $lot_size * $kucoin_price * $usdt_price) {
                    Log::info('irr not sufficient *** pair_id='.$pair->pair_id);
                    return false;
                }
            } elseif ($quote_currency_id == 9) {
                if ($usdt_balance_account < 10 * $lot_size * $kucoin_price) {
                    Log::info('usdt not sufficient *** pair_id='.$pair->pair_id);
                    return false;
                }
            } else {
                Log::info('quote_currency_id is not irr or usdt =>' . $quote_currency_id);
                return false;
            }

        } else {
            Log::info('side is not buy or sell =>' . $side);
            return false;
        }
//        Log::info('balance is sufficient');
        return true;
    }

    public function getPackNum($pair)
    {
        $rmzx_currency_id = $pair->base_currency_id;
        $currency_pack_num = ExchangeCurrency::query()->where('currency_id', '=', $rmzx_currency_id)->where('exchange_name', '=', 'Ramzinex')->pluck('pack_num');
        return $currency_pack_num[0];

    }

    public function getCurrencyBalances(int $currency_id, $account_id)
    {
        if ($account_id == 'kucoin') {
            if (Cache::has('account-kucoin-currency:' . $currency_id . '-balance')) {
                return Cache::get('account-kucoin-currency:' . $currency_id . '-balance');
            }
        } else {
            if (Cache::has('main_balance_check' . $currency_id . '_' . $account_id)) {
                return Cache::get('main_balance_check' . $currency_id . '_' . $account_id);
            }
        }

//        $redis = Redis::connection('V2');
//        $balances = $redis->get('balances');
//        if ($balances) {
//            Log::info('getCurrencyBalances =>inside if');
//            return $balances[$currency_id][$account_id];
//        }
//        return -1;
//        if (Cache::has('account-' . $account_id . '-currency:' . $currency_id . '-balance')) {
//            return Cache::get('account-' . $account_id . '-currency:' . $currency_id . '-balance');
//        }
        return -1;
    }

    /**
     * @param int $pair_id
     * @param string $side
     * @param int $account_id
     * @return array|int
     */
    public function getPairOpenOrders(int $pair_id, string $side, int $account_id)
    {
//        $redis = Redis::connection('V2');

        if (Cache::has('open_orders')) {
            $open_orders = Cache::get('open_orders');
            $account_open_orders = $open_orders[$account_id];
//            Log::info($account_open_orders);
            if ($account_open_orders === false) {
                Log::info('open order is false');
                return -1;
            }

            $filtered_open_orders = [];
            foreach ($account_open_orders as $open_order) {
                if ($open_order['pair_obj']['pair_id'] == $pair_id and $open_order['type_en'] == $side) {
                    $filtered_open_orders[] = $open_order;
                }
            }
            return $filtered_open_orders;

        } else {
            Log::info('problem in getting open orders getPairOpenOrders-getPairOpenOrders');
            $this->cacheOpenOrders();
            return -1;
        }
    }


    public function getOpenOrders()
    {
        if (Cache::has('open_orders')) {
            return Cache::get('open_orders');
        } else {
            Log::info('problem in getting open orders getPairOpenOrders-getOpenOrders');
            $this->cacheOpenOrders();
            return -1;
        }
    }


    public function cacheOpenOrders()
    {
        $open_orders = MainManagerV2::getInstance()->getAllAccountsOpenOrders(1000);
        if (!$open_orders) {
            $open_orders = MainManagerV2::getInstance()->getAllAccountsOpenOrders(1000);
        }
        $cached = Cache::put('open_orders', $open_orders, 120);
//        $cached = Cache::put('open_orders', $open_orders);
        if ($cached) {
            return 1;
        } else {
            return -1;
        }
    }

    public function updateRamzinexOrders($db_open_order)
    {
//        Log::info($db_open_order);
        $order_id = $db_open_order->order_id;
//        Log::info('ORDER ID');
//        Log::info($order_id);

        $bot = BotConfig::query()->find($db_open_order->bot_id);
        $order = RamzinexManager::getInstance()->getOneOrder($order_id, $bot);


        try {
            $filled = $order['filled_nr'];
            $percent = $order['percent_num'];

            if ($filled != null) {
                $db_open_order->state = 2;
                $db_open_order->filled_volume = $filled;
                $db_open_order->filled_percent = $percent;
            } else {
                $db_open_order->state = 3;
            }
            $db_open_order->updated_at = Carbon::now();
            $db_open_order->save();
        } catch (\Exception $exception) {
            Log::info('problem in saving changes of order in database !!!');
        }


    }

    /**
     * @return int
     */
    public function cacheBalances(): int
    {
        $this->deleteCachedBalances();
        $main_balances = MainManagerV2::getInstance()->getAllAccountsBalanceSummary();
        if ($main_balances == false) {
            Log::info('problem in caching balances**');
            return false;
        }
        $kucoin_balances = KucoinManager::getInstance()->getBalance();

        $balances = BalanceHelper::getInstance()->getAllBalances($main_balances, $kucoin_balances);

        foreach ($balances as $currency_id => $balance) {
            if (isset($balance['kucoin'])) {
                Cache::put('account-kucoin-currency:' . $currency_id . '-balance', $balance['kucoin'], 60);
            }
        }
        Cache::put('account-kucoin-currency:' . 2 . '-balance', 0);

        return 1;

    }

    public function deleteCachedBalances()
    {
        $currency_ids = keepBalanceSteady::getInstance()->getCurrencies();
        foreach ($currency_ids as $currency) {
            Cache::forget('account-1-currency:' . $currency->main_currencyID . '-balance');
            Cache::forget('account-2-currency:' . $currency->main_currencyID . '-balance');
            if (isset($balance['kucoin'])) {
                Cache::forget('account-kucoin-currency:' . $currency->main_currencyID . '-balance');
            }
        }
    }

    public function getAccounts()
    {
        return Account::query()->get();
    }

    public function getBotConfigs(int $enable = null)
    {
        return BotConfig::query()->when(isset($enable), function ($query) use ($enable) {
            $query->where('enable', '=', $enable);
        })->get();
    }

    /**
     * @param int $pair_id
     */
    public function getPairById(int $pair_id)
    {
//        Log::info('check getPairById ');
//        Log::info($pair_id);
        return Pair::query()->where('pair_id', '=', $pair_id)->first();
    }


    public function cancelOrders()
    {
//        Log::info('check cancelOrders');
        $open_orders = $this->getOpenOrders();
        if (!$open_orders) {
            Log::info('there is no open order in cache ');
        }
        $db_open_orders = $this->getDataBaseOpenOrders();
        foreach ($open_orders as $account_id => $account_open_orders) {
            foreach ($account_open_orders as $open_order) {
                $db_open_order = $this->findOpenOrderInDb($db_open_orders, $open_order);

                if ($db_open_order === -1) {

                    $db_order = $this->findOpenOrderById($open_order['id']);
                    if ($db_order == null || $db_order === -1) {
//                        $this->createOrder($db_order, $account_id);
                        $db_order = $this->createOrder($open_order, $account_id);
                    } else {
                        try {
                            $this->changeOrderState($open_order['id']);
                        } catch (Exception $e) {
                            Log::info('problem in changeOrderState**');
                        }
                    }
//                    Log::info('DB ORDER');
//                    Log::info($db_order);
//                    Log::info($db_order->account_id);
                    $this->cancelOpenOrder($open_order, $db_order[0]->bot_id);
//                    $this->cancelOpenOrder($open_order, 0,$open_order['id']);
//                    $this->updateRamzinexOrders($open_order, $db_order);

                } else {
                    $shouldBeCanceled = $this->shouldBeCanceled($open_order, $db_open_order, $account_id);
                    if ($shouldBeCanceled) {
                        $this->cancelOpenOrder($open_order, $db_open_order->bot_id);
//                        Log::info($db_open_order);
//                        $this->cancelOpenOrder($open_order, 0,$db_open_order->order_id);
//                        sleep(.5);
//                        $this->updateRamzinexOrders($open_order, $db_open_order);
                    }
                }

            }
        }
    }


    public function createOrder($order, $account_id)
    {
//        Log::info('check createOrder ');
        $bot_id = $this->getBotConfigIdByPair(intval($order['pair_obj']['pair_id']));
        $db_order = new Order();
        $db_order->order_id = $order['id'];
        $db_order->base_currency_id = $order['pair_obj']['base_currency_id'];
        $db_order->account_id = $account_id;
        $db_order->quote_currency_id = $order['pair_obj']['quote_currency_id'];
        $db_order->base_precision = 0;
        $db_order->quote_precision = 0;
        $db_order->amountA = floatval($order['amount']);
        $db_order->amountB = floatval($order['amount_quote_nr']);
        $db_order->side = $order['type_en'];
        $db_order->execute_price = floatval($order['order_price']);  //??
        $db_order->bot_execute_price = 0;
        $db_order->filled_percent = 0;
        $db_order->filled_volume = 0;
        $db_order->high_ok_price = 0;
        $db_order->low_ok_price = 0;
        $db_order->state = 1;
        $db_order->bot_id = $bot_id;
        $db_order->created_unix = time();
        $db_order->created_at = Carbon::now();
        $db_order->updated_at = Carbon::now();
        try {
            $db_order->save();
            return $db_order;
        } catch (\Exception|QueryException $exception) {
            Log::info('problem in creating order');
            return false;
        }


    }

//    public function cancelOpenOrder($open_order, $bot_id, $order_id)
    public function cancelOpenOrder($open_order, $bot_id)
    {
//        Log::info('check cancelOpenOrder');
//        $bot_id2 = RamzinexOrder::query()->where('order_id', '=', $order_id)->pluck('bot_id');
//        $bot = $this->getBot($bot_id2[0]);
        $bot = $this->getBot($bot_id);

        $is_canceled = RamzinexManager::getInstance()->cancelOrder($open_order['id'], $bot);
        if (!$is_canceled) {
            Log::info('problem in canceling order of tradeBotManagerV2 =>' . $open_order['id']);
        }

    }

    public function changeOrderState($order_id)
    {
//        Log::info('check changeOrderState');
        $is_updated = Order::query()->where('order_id', '=', $order_id)->update(['state' => 1]);
        if (!$is_updated) {
            Log::info('problem in updating order state' . $order_id);
        }

    }

    public function getBot($bot_id)
    {
//        Log::info('check getBot');
        return BotConfig::query()->where('id', '=', $bot_id)->first();

    }

    public function getBotConfigIdByPair(int $pair_id)
    {
//        Log::info('check getBotConfigIdByPair');
        return BotConfig::query()->where('pair_id', '=', $pair_id)->first()->id;
    }

    public function getBotConfigByPairId($pair_id)
    {
        return BotConfig::query()->where('pair_id', '=', $pair_id)->first();
    }

    public function shouldBeCanceled($open_order, $db_open_order, $account_id)
    {
//        Log::info('check shouldBeCanceled');
        $high_ok_price = $db_open_order->high_ok_price;
        $low_ok_price = $db_open_order->low_ok_price;
        $live_price = $this->getKucoinPriceFromCache($open_order['pair_id']);
//        Log::info($live_price);
        if ($low_ok_price >= $live_price || $high_ok_price <= $live_price) {
            Log::info("CANCEL PRICE");

            return true;
        }
        $filled_percent = $this->calculateFilledPercent($open_order);
        if ($filled_percent > 50) {
            Log::info("CANCEL FILL");
            return true;
        }

        $pair_open_orders = $this->getPairOpenOrders($open_order['pair_obj']['pair_id'], $open_order['type_en'], $account_id);
        if (Count($pair_open_orders) > 1) {
            Log::info("CANCEL MORE THAN ONE ");
            return true;
        }
        $now = time();
//        Log::info($now);
//        Log::info($now - $open_order['created_at_ms']);

        if ($now - $open_order['created_at_ms'] > 10 * 60) {
            Log::info("CANCEL TIME");

            return true;
        }
        return false;
    }

    public function calculateFilledPercent($order)
    {
//        Log::info('check calculateFilledPercent');
        $order_volume = $order['amount_nr'];
        $filled = $order['filled_nr'];
        $filled_percent = $filled / $order_volume * 100;
        return $filled_percent;
    }


    public function getKucoinPriceFromCache(int $pair_id)
    {
//        Log::info('check getKucoinPriceFromCache');

        $pair = $this->getPairById($pair_id);
        $price = Cache::get('price_' . $pair->symbol);

        if ($price) {
            return $price;
        }
        return KucoinManager::getInstance()->getPrice($pair);


    }

    public function findOpenOrderInDb($db_open_orders, $open_order)
    {
//        Log::info('check findOpenOrderInDb');
        foreach ($db_open_orders as $db_open_order) {
            if ($db_open_order->order_id == intval($open_order['id'])) {
                return $db_open_order;
            }
        }
        return -1;
    }


    public function getDataBaseOpenOrders()
    {
//        Log::info('check getDataBaseOpenOrders');
        if (Order::query()->where('state', '=', 1)->exists()) {
            return Order::query()->where('state', '=', 1)->get();
        }
        return -1;
    }

    public function findOpenOrderById(int $order_id)
    {
//        Log::info('check findOpenOrderById');
        if (Order::query()->where('order_id', '=', $order_id)->exists()) {
            return Order::query()->where('order_id', '=', $order_id)->get();
        } else {
            return -1;
        }
    }

}
