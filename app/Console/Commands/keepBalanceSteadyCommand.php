<?php

namespace App\Console\Commands;

use App\Jobs\KeepBalanceSteadyJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;

class keepBalanceSteadyCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'keepBalance:now';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'keepBalance:now';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the job for calculating user pnl and store in portfolio_users table.
     *
     * @return mixed
     */
    public function handle()
    {
        KeepBalanceSteadyJob::dispatch()->onQueue('test');

    }


}
