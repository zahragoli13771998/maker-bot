<?php

namespace App\Console\Commands;

use App\Jobs\cacheJob;
use App\Jobs\updateRamzinexOrdersJob;
use App\Logic\ChangeCurrencyManager;
use App\Logic\keepBalanceSteady;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;


class updateOrder extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateRamzinexOrder:now';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'updateRamzinexOrder:now';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the job for calculating user pnl and store in portfolio_users table.
     *
     * @return mixed
     */
    public function handle()
    {
        updateRamzinexOrdersJob::dispatch()->onQueue('test');
    }
}
//laravel_database_laravel_cache:
