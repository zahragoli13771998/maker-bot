<?php

namespace App\Console\Commands;

use App\Jobs\cacheJob;
use App\Jobs\KucoinCacheJob;
use App\Logic\CacheManager;
use App\Logic\keepBalanceSteady;
use App\Logic\KucoinManager;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;


class kucoinCacheCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kucoinCache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'kucoinCache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the job for calculating user pnl and store in portfolio_users table.
     *
     * @return mixed
     */
    public function handle()
    {

        KucoinCacheJob::dispatch()->onQueue('test');
    }
}
