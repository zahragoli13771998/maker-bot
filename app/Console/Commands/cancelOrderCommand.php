<?php

namespace App\Console\Commands;

use App\Jobs\CancelOrdersV2Job;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;

class cancelOrderCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cancelOrderCommandV2:now';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cancelOrderCommandV2:now';

    /**
     * Create a new command instance.
     *
     * @return void
     **/

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the job for calculating user pnl and store in portfolio_users table.
     *
     * @return mixed
     */
    public function handle()
    {

        CancelOrdersV2Job::dispatch()->onQueue('test');

    }


}