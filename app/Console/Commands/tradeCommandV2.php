<?php

namespace App\Console\Commands;

use App\Jobs\cacheJob;
use App\Jobs\CancelOrdersV2Job;
use App\Jobs\SubmitOrdersJob;
use App\Jobs\tradesJobV2;
use App\Logic\tradeBotManagerV2;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;

class tradeCommandV2 extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tradeCommandV2:now';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'tradeCommandV2:now';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the job for calculating user pnl and store in portfolio_users table.
     *
     * @return mixed
     */
    public function handle()
    {

        SubmitOrdersJob::dispatch()->onQueue('test');


    }
}
