<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //cache
        $schedule->command('cacheCommandV2:now')->everyMinute();
        $schedule->command('kucoinCache')->everyMinute();
        sleep(2);
        $schedule->command('cancelOrderCommandV2:now')->everyMinute();
        sleep(2);
        $schedule->command('cacheCommandV2:now')->everyMinute();

        $schedule->command('tradeCommandV2:now')->everyMinute();
        $schedule->command('cacheCommandV2:now')->everyMinute();

        $schedule->command('keepBalance:now')->everyMinute();
        $schedule->command('updateOrder:now')->everyMinute();


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
