<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKucoinOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('KucoinOrder', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('uuid');
            $table->string('order_id')->index();
            $table->string('symbol');
            $table->double('size');
            $table->double('fund');
            $table->double('deal_fund');
            $table->double('deal_size');
            $table->string('side');
            $table->string('type');
            $table->double('fee');
            $table->string('fee_currency');
            $table->string('reference_order_id');
            $table->unsignedBigInteger('unix_created_at_kucoin');
            $table->integer('state');
            $table->integer('quote_currency_id');
            $table->integer('tether_change_state');
            $table->integer('account_id');
            $table->integer('residual_check_state');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('KucoinOrder');
    }
}
