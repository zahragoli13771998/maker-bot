<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('ramzinex_currencyID')->index();
            $table->string('currency_symbol')->index();
            $table->double('balance');
            $table->integer('ramzinex_volume_precision');
            $table->double('LFAT');
            $table->double('balance_range_lower_bound');
            $table->double('balance_range_upper_bound');
            $table->integer('enable');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
