<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangeCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ExchangeCurrency', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('currency_id')->index();
            $table->string('symbol');
            $table->string('exchange_name');
            $table->integer('exchange_id')->index();
            $table->integer('volume_precision');
            $table->integer('price_precision');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ExchangeCurrency');
    }
}
