<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePairTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pair', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('pair_id')->index();
            $table->string('base_ENname')->index();
            $table->string('base_FAname')->index();
            $table->string('quote_ENname')->index();
            $table->string('quote_FAname')->index();
            $table->integer('base_currency_id')->index();
            $table->integer('quote_currency_id')->index();
            $table->integer('base_precision')->index();
            $table->integer('quote_precision')->index();
            $table->string('symbol')->index();
            $table->integer('commission_maker');
            $table->integer('commission_taker');
            $table->integer('exchange_id');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Pair');
    }
}
