<?php

use Cassandra\Bigint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Nette\Utils\DateTime;

class CreateBotConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('BotConfig', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('pair_id')->index();
            $table->string('name');
            $table->integer('exchange_id_1');
            $table->integer('exchange_id_2');
            $table->double('lot_size');
            $table->double('spread');
            $table->integer('threshold_enable');
            $table->double('threshold');
            $table->integer('time_interval');
            $table->integer('enable');
            $table->integer('enable_last_state');
            $table->double('daily_max_trade_limit');
            $table->integer('account_id');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('BotConfig');
    }
}
