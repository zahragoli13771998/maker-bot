<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawDepositsCheckpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraw_deposits_checkpoints', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('currency_id');
            $table->float('amount');
            $table->bigInteger('time');
            $table->string('account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraw_deposits_checkpoints');
    }
}
