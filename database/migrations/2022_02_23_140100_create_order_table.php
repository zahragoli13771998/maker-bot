<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRamzinexOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Ramzinex_order', function (Blueprint $table) {
            $table->id()->index();
            $table->integer('order_id');
            $table->integer('account_id');
            $table->integer('base_currency_id');
            $table->integer('quote_currency_id');
            $table->integer('base_precision');
            $table->integer('quote_precision');
            $table->double('amountA');
            $table->double('amountB');
            $table->string('side');
            $table->double('execute_price');
            $table->double('bot_execute_price');
            $table->double('filled_percent');
            $table->double('filled_volume');
            $table->double('high_ok_price');
            $table->double('low_ok_price');
            $table->integer('state');
            $table->integer('bot_id');
            $table->unsignedBigInteger('created_unix');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Ramzinex_order');
    }
}
